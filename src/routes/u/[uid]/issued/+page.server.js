import { invalid } from '@sveltejs/kit';
import { getAccountBundleByIdOrName } from '../../../../helpers/nft_helper';

/** @type {import('./$types').PageServerLoad} */
export async function load({ params }) {
    let account_full = null;
    account_full = await getAccountBundleByIdOrName(params.uid);

    if (account_full == null) {
        return invalid(404, { username: 'No user with this username' });
    } else {
        return {
            account_full
        };
    }
}
