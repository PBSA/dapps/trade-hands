import { getAccountBundleByIdOrName } from '../../../helpers/nft_helper';
import { invalid, json } from '@sveltejs/kit';

/** @type {import('./$types').PageServerLoad} */
export async function load({ params }) {
    let account_full = null;
    account_full = await getAccountBundleByIdOrName(params.uid);

    if (account_full == null) {
        return invalid(404, { username: 'No user with this username' });
    } else {
        return {
            // retrieve a specific header
            account_full
        };
    }
}
