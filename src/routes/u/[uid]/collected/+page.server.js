import {
    getAccountBundleByIdOrName,
    getNftAssetBundlesByOwnerID
} from '../../../../helpers/nft_helper';
import { invalid } from '@sveltejs/kit';

/** @type {import('./$types').PageServerLoad} */
export async function load({ params }) {
    let account_full = null;
    let user_nft_list = null;
    account_full = await getAccountBundleByIdOrName(params.uid);
    if (account_full == null) {
        return {
            status: 404
        };
    } else {
        var instance_id = account_full.id.split('.')[2];
        var user_id_or_name = params.uid;
        user_nft_list = await getNftAssetBundlesByOwnerID(instance_id);
        return {
            account_full,
            user_nft_list,
            user_id_or_name
        };
    }
}
