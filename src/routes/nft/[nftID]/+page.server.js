import { error } from '@sveltejs/kit';
import { getNftAssetBundleByID } from '../../../helpers/nft_helper';

/** @type {import('./$types').PageServerLoad} */
export async function load({ params }) {
    const nft_full = await getNftAssetBundleByID(params.nftID);
    if (!nft_full) {
        throw error(404, 'Not found');
    } else {
        let HTMLDATA = { nft_full: nft_full };
        return {
            HTMLDATA
        };
    }
}
