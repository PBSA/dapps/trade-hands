import {
    getNftAssetBundlesByMetadataID,
    getNftMetadataBundleByID
} from '../../../../helpers/nft_helper';

/** @type {import('./$types').PageServerLoad} */
export async function load({ params }) {
    let HTMLDATA = null;
    HTMLDATA = await getNftAssetBundlesByMetadataID(params.nftID);
    let metadata = await getNftMetadataBundleByID(params.nftID);
    if (!HTMLDATA) {
        return {
            status: 404
        };
    } else {
        return {
            HTMLDATA: HTMLDATA,
            metadata: metadata,
            nftID: params.nftID
        };
    }
}
