import { blockchainUrl } from '../config/blockchain';
import { Apis } from 'peerplaysjs-lib/dist/index.js';

/**
 * @param {Array} ids
 * @param {any} db_api
 * @param {any} known_names (in/out)
 */
async function getAccountNamesFromIDs(ids, db_api, /* in/out */ known_names = {}) {
    let account_ids = [...new Set(ids)]; // (deduplicate ids)
    let lookup_ids = [];
    for (const id of account_ids) {
        if (!(id in known_names)) {
            lookup_ids.push(id);
        }
    }
    console.log('Requesting ', account_ids, ' looking up ', lookup_ids);
    let names = {};
    if (lookup_ids.length > 0) {
        await db_api.exec('get_objects', [lookup_ids]).then((/** @type {any} */ r) => {
            for (const acct of r) {
                known_names[acct.id] = acct.name;
            }
        });
    }
    for (const id of account_ids) {
        if (id in known_names) {
            names[id] = known_names[id];
        }
    }
    return names;
}

/**
 * @param {Array} mdId
 * @param {any} db_api
 * @param {any} known_metadata (in/out)
 */
async function getNftMetadataFromID(mdId, db_api, /* in/out */ known_metadata = {}) {
    if (!/^[1-2]\.[0-9]+\.[0-9]+$/.test(mdId)) {
        console.log('Bad Metadata ID: ', mdId);
        return null;
    } // ^^ TODO: Is this best place to catch bad object ID?
    if (!(mdId in known_metadata)) {
        await db_api.exec('get_objects', [[mdId]]).then((/** @type {any} */ r) => {
            known_metadata[mdId] = { ...r[0] };
            console.log('Retrieved metadata object ' + mdId);
        });
    }
    if (mdId in known_metadata) {
        return known_metadata[mdId];
    } else {
        return null;
    }
}

/**
 * Return a complete NFT Metadata bundle by instance ID.  Bundle
 * includes the metadata object, name mappings, and selected rpc
 * replies.
 *
 * @param {any} instance_id - The instance ID of the metadata object.
 @ @returns {(Object|null)}
 */
async function getNftMetadataBundleByID(instance_id) {
    const obj_id = '1.30.' + instance_id;
    let Bundle = null;
    await Apis.instance(blockchainUrl, true)
        .init_promise.then(async () => {
            const db_api = Apis.instance().db_api();
            await db_api.exec('get_objects', [[obj_id]]).then(async (/** @type {any} */ r) => {
                let nft_md_obj = r[0];
                Bundle = await assembleNftMetadataBundle(nft_md_obj, db_api);
            });
        })
        .then(() => {
            Apis.instance().close();
            console.log('connection closed!');
        })
        .catch((/** @type {any} */ error) => {
            Apis.instance().close();
            console.error(error);
        });

    if (!Bundle) {
        console.log('Could not retrieve assets for ' + obj_id + '.');
    }
    console.log('Returning: NFT Metadata Bundle = ', Bundle);
    return Bundle;
}

/**
 * Given an NFT metadata object already retrieved from chain, assemble
 * it into a bundle structure, retrieving any supporting chain objects
 * or data, and return the bundle.
 *
 * @param {Object} nft_md_obj - NFT metadata object (a 1.30.x object)
 * @param {any} db_api - Preinitialized database API
 * @param {Object} known_names - Names we already know.
 */
async function assembleNftMetadataBundle(nft_md_obj, db_api, /* in/out */ known_names = {}) {
    let Bundle = {
        id: nft_md_obj.id,
        objects: {},
        rpc: {}
    };
    Bundle.objects[nft_md_obj.id] = { ...nft_md_obj };
    let account_ids = [
        nft_md_obj.owner,
        ...(nft_md_obj.revenue_partner ? [nft_md_obj.revenue_partner] : [])
    ];
    let names = await getAccountNamesFromIDs(account_ids, db_api, known_names);
    Bundle.names = names;

    let rpcs = ['nft_get_total_supply'];
    for (const rpc of rpcs) {
        await db_api.exec(rpc, [nft_md_obj.id]).then(async (/** @type {any} */ r) => {
            Bundle.rpc[rpc] = r;
        });
    }

    // Resolve lotto options:
    if (nft_md_obj.lottery_data) {
        await db_api
            .exec('get_objects', [[nft_md_obj.lottery_data.lottery_balance_id]])
            .then(async (/** @type {any} */ r) => {
                Bundle.objects[nft_md_obj.lottery_data.lottery_balance_id] = r[0];
            });
    }

    return Bundle;
}

/**
 * @param {any} nft_obj  NFT object (a 1.31.x object)
 * @param {any} db_api   Preinitialized database API
 * @param {Object} known_names Names we already know.
 * @param {Object} known_metadata Metadata we already know.
 */
async function getNftAssetBundle(
    nft_obj,
    db_api,
    /* in/out */ known_names = {},
    /* in/out */ known_metadata = {}
) {
    var objectAtId = {};
    if (nft_obj.id) {
        objectAtId[nft_obj.id] = nft_obj;
    }

    let Bundle = {
        id: nft_obj.id,
        objects: {}
    };
    let metadata_obj = await getNftMetadataFromID(nft_obj.nft_metadata_id, db_api, known_metadata);
    let account_ids = [
        metadata_obj.owner,
        nft_obj.owner,
        nft_obj.approved,
        ...nft_obj.approved_operators
    ];
    let names = await getAccountNamesFromIDs(account_ids, db_api, known_names);
    Bundle.objects[nft_obj.id] = { ...nft_obj };
    Bundle.objects[metadata_obj.id] = { ...metadata_obj };
    Bundle.names = names;
    // TODO: Account name extraction is incomplete. Perhaps, now that
    // we put all objects in an object dictionary, we could traverse
    // it to extract names.

    return Bundle;
}

/**
 * @param {any} _nftID
 */
async function getNftAssetBundleByID(_nftID) {
    const objPrefix = '1.31.';
    let Bundle = null;
    await Apis.instance(blockchainUrl, true)
        .init_promise.then(async () => {
            const db_api = Apis.instance().db_api();
            await db_api
                .exec('get_objects', [[objPrefix + _nftID]])
                .then(async (/** @type {any} */ r) => {
                    let nft_obj = r[0];
                    Bundle = await getNftAssetBundle(nft_obj, db_api);
                });
        })
        .then(() => {
            Apis.instance().close();
            console.log('connection closed!');
        })
        .catch((/** @type {any} */ error) => {
            Apis.instance().close();
            console.error(error);
        });

    if (!Bundle) {
        console.log('Could not retrieve NFT assets for ' + objPrefix + _nftID + '.');
    }
    console.log('Returning: NFT Asset Bundle = ', Bundle);
    return Bundle;
}

/**
 * @param {any} _nftID
 */
async function getNftAssetBundlesByMetadataID(_nftmID) {
    const objPrefix = '1.30.';
    let BigBundle = null;
    let metadata = null;
    let nft_list = [];
    let known_names = {};
    let known_metadata = {};
    await Apis.instance(blockchainUrl, true)
        .init_promise.then(async () => {
            const db_api = Apis.instance().db_api();
            metadata = await getNftMetadataFromID(objPrefix + _nftmID, db_api, known_metadata);
            BigBundle = { metadata: { ...metadata } };
            let account_ids = [metadata.owner];
            if (metadata.revenue_partner) account_ids.push(metadata.revenue_partner);
            known_names = await getAccountNamesFromIDs(account_ids, db_api);
            BigBundle.names = { ...known_names };
            let nft_count = 0;
            await db_api
                .exec('nft_get_total_supply', [metadata.id])
                .then(async (/** @type {any} */ r) => {
                    nft_count = r;
                });
            for (let index = 0; index < nft_count; index++) {
                await db_api
                    .exec('nft_token_by_index', [metadata.id, index])
                    .then(async (/** @type {any} */ r) => {
                        let nft_obj = r;
                        let Bundle = await getNftAssetBundle(
                            nft_obj,
                            db_api,
                            known_names,
                            known_metadata
                        );
                        Bundle.index = index;
                        nft_list.push(Bundle);
                    });
            }
            BigBundle.nft_list = nft_list;
        })
        .then(() => {
            Apis.instance().close();
            console.log('connection closed!');
        })
        .catch((/** @type {any} */ error) => {
            Apis.instance().close();
            console.error(error);
        });

    if (!BigBundle) {
        console.log('Could not retrieve NFT assets for ' + objPrefix + _nftmID + '.');
    }
    console.log('Returning: NFT Asset Bundle Pack = ', BigBundle);
    return BigBundle;
}

/**
 * Get an array of NFT "bundles" where each bundle packages an NFT
 * object along with supporting objects and data. This one returns all
 * NFTs belonging to (held by) a specified account.
 *
 * @param {string} _accID - Instance ID of the account to be querried;
 * non-negative integer as a decimal string.
 *
 * @returns {(Object[]|null)} - Array of NFT bundles
 */
async function getNftAssetBundlesByOwnerID(_accID) {
    let BigBundle = null;
    let nft_list = [];
    let known_names = {};
    let known_metadata = {};
    await Apis.instance(blockchainUrl, true)
        .init_promise.then(async () => {
            const db_api = Apis.instance().db_api();
            BigBundle = {};
            let nft_count = 0;
            let nft_objs = [];
            await db_api
                .exec('nft_get_tokens_by_owner', ['1.2.' + _accID])
                .then(async (/** @type {any} */ r) => {
                    nft_objs = r;
                    nft_count = nft_objs.length;
                });
            for (let index = 0; index < nft_count; index++) {
                let nft_obj = nft_objs[index];
                let Bundle = await getNftAssetBundle(nft_obj, db_api, known_names, known_metadata);
                nft_list.push(Bundle);
            }
            BigBundle.nft_list = nft_list;
        })
        .then(() => {
            Apis.instance().close();
            console.log('connection closed!');
        })
        .catch((/** @type {any} */ error) => {
            Apis.instance().close();
            console.error(error);
        });

    if (!BigBundle) {
        console.log('Could not retrieve NFT assets for account 1.2.' + _accID + '.');
    }
    console.log('Returning: NFT Asset Bundle Pack = ', BigBundle);
    return BigBundle;
}

/**
 * Retrieve an account object and return a bundle containing the
 * object and any supporting objects or data.
 *
 * @param {string} _acc - Account name or instance ID as a string
 *
 * @returns {(Object|null)} - Account bundle
 */
async function getAccountBundleByIdOrName(_acc) {
    const _acc_is_id = /^[0-9]+$/.test(_acc);
    const _acc_is_name = !_acc_is_id && /^[a-z][0-9a-z.-]+$/.test(_acc);
    const _acc_is_valid = _acc_is_name || _acc_is_id;
    let Bundle = null;
    if (_acc_is_valid) {
        await Apis.instance(blockchainUrl, true)
            .init_promise.then(async () => {
                const db_api = Apis.instance().db_api();
                let rpc_call = _acc_is_name ? 'lookup_account_names' : 'get_objects';
                let rpc_arg = _acc_is_name ? _acc : '1.2.' + _acc;
                await db_api.exec(rpc_call, [[rpc_arg]]).then(async (/** @type {any} */ r) => {
                    let acc_obj = r[0];
                    Bundle = await getAccountBundle(acc_obj, db_api);
                });
            })
            .then(() => {
                Apis.instance().close();
                console.log('connection closed!');
            })
            .catch((/** @type {any} */ error) => {
                Apis.instance().close();
                console.error(error);
            });
    }
    if (!Bundle) {
        console.log('Could not retrieve Account assets for ' + _acc + '.');
    }
    console.log('Returning: Account Bundle = ', Bundle);
    return Bundle;
}

/**
 * Bundle up an account object with supporting chain objects and other
 * data, and return the bundle.
 *
 * @param {Object} acc_obj - An account object (a 1.2.x object)
 * @param {Object} db_api - Preinitialized database API
 * @param {Object} known_names - Dictionary of names we already know.
 *
 * @returns {Object}
 */
async function getAccountBundle(acc_obj, db_api, /* in/out */ known_names = {}) {
    var objectAtId = {};
    if (acc_obj.id) {
        objectAtId[acc_obj.id] = acc_obj;
        let Bundle = {
            id: acc_obj.id,
            objects: objectAtId
        };
        let account_ids = [
            acc_obj.registrar,
            acc_obj.referrer,
            acc_obj.lifetime_referrer,
            acc_obj.options.voting_account
        ];
        let names = await getAccountNamesFromIDs(account_ids, db_api, known_names);
        Bundle.names = names;

        return Bundle;
    }
}

/**
 * @param {string} json_uri
 */
function getImageLinkfromjson(json_uri) {
    console.log(json_uri);
    var url = '';
    try {
        var json = JSON.parse(json_uri);
        for (const key in json) {
            if (key == 'image') {
                var temp = json[key];
                break;
            }
            if (json[key].includes('ipfs')) {
                var temp = json[key];

                break;
            }
        }
        temp = temp.replace('/ipfs/', 'https://tradehands.peerplays.download/ipfs/');
        url = temp.replace('ipfs://', 'https://tradehands.peerplays.download/ipfs/');

        return url;
    } catch (error) {
        return '';
    }
}

export {
    getNftMetadataBundleByID,
    getNftAssetBundleByID,
    getImageLinkfromjson,
    getNftAssetBundlesByMetadataID,
    getNftAssetBundlesByOwnerID,
    getAccountBundleByIdOrName
};
