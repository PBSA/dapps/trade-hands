# TradeHands / TradeHands Explorer

TradeHands Explorer is an NFT-centric block explorer. It is used to view NFT related objects on the Peerplays blockchain. It will be extendable with modules to add additional functionality, from which more complex applications may be built.

## Getting Started

### Build with:

TradeHands is a JS app (Node.js), built with the [Svelte UI framework](https://svelte.dev), and the [Vite](https://vitejs.dev) build tools.

### Prerequisites:

Node/NPM version 16 or higher is needed, and will need to be installed if not already included on your system. To ensure node 16 is installed - You can use the following commands on Ubuntu which are provided by https://github.com/nodesource/distributions

```
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash - &&\
sudo apt-get install -y nodejs
```

### Installation:

First clone the repo. Next, install packages with npm.

```bash
git clone https://gitlab.com/PBSA/dapps/trade-hands.git
cd trade-hands
npm install
```

### Running the dev server:

The dev server can be run with:

```
npm run dev
```

and will listen on http://localhost:3000/. If you need it to listen on all interfaces (not just localhost), run with this command:

```
npm run dev -- --host 0.0.0.0
```

The port can also be changed with `--port [port]`.

### Running in production:

For this, build first then "preview" (serves the build app).

```
npm run build
npm run preview -- --host 0.0.0.0 --port [port]
```

### Reaching the app:

Simply point browser at host:port, e.g. http://localhost:3000/.

Optional: An NGINX Reverse Proxy can/should be set up to secure the connection with SSL.

## Usage

The TradeHands Explorer will be able to

-   View detailed information on NFTs
-   Show NFT collections by issuing contracts
-   Show NFTs issued/held by accounts
-   Show market activity

## Roadmap

In the TradeHands marketplace, the TradeHands explorer module is the basic application which is planned to be extendable with modular functionality to create more advanced applications such as a full marketplace.

The below add-on modules are planned in the TradeHands component library,

1.  User Login/User site data
2.  Market Analytics and activity
3.  Front page tools
4.  User pages and galleries
5.  NFT creator tools

## License

MIT
